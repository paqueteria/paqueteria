## Descripción

Paquetería project

## Instalación

Para correr el proyecto se necesita instalar docker y docker-compose

```bash
# Bajar el repo de git
$ git clone git@gitlab.com:paqueteria/paqueteria.git

# Navegar a la carpeta del proyecto
$ cd paqueteria

# Inicializar los submodulos de git
$ git submodule update --init --recursive
```

## Correr la aplicación

```bash
$ docker-compose up --build -d

```

## Puertos

```
front => localhost:4200
api => localhost:3000
mongo => localhost:27017 (usuario: root, password: paqueteriaPassword)
```
